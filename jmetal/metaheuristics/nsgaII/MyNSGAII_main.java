package jmetal.metaheuristics.nsgaII;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import static jmetal.metaheuristics.nsgaII.NSGAII_mTSP_main.logger_;
import static jmetal.metaheuristics.nsgaII.NSGAII_main.fileHandler_;
import static jmetal.metaheuristics.nsgaII.NSGAII_main.logger_;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.problems.mgalv.FeatureSelection;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.Configuration;
import jmetal.util.JMException;


public class MyNSGAII_main {
    public static Logger      logger_ ;      // Logger object
    public static FileHandler fileHandler_ ; // FileHandler object
    
    public static void main(String[] args) throws IOException, JMException, ClassNotFoundException {
        Problem problem;
        Algorithm algorithm;
        Operator crossover;
        Operator mutation;
        Operator selection;
        
        HashMap parameters;
        
        QualityIndicator indicators;
        
        logger_      = Configuration.logger_ ;
        fileHandler_ = new FileHandler("NSGAII_main.log"); 
        logger_.addHandler(fileHandler_) ;
        
        indicators = null;
        
        String path = "/home/matheus/Documentos/IA/datasets/large scale/nursery.arff";
        
        problem = problem = new FeatureSelection(path);
        
        algorithm = new NSGAII(problem);
        algorithm.setInputParameter("populationSize",100);
        algorithm.setInputParameter("maxEvaluations",2000);
        
        //selection
        parameters = null;
        selection = SelectionFactory.getSelectionOperator("BinaryTournament", parameters);
        
        //crossover
        parameters = new HashMap();
        parameters.put("probability", 0.9);
        crossover = CrossoverFactory.getCrossoverOperator("SinglePointCrossover", parameters);
        
        //mutation
        parameters = new HashMap();
        parameters.put("probability", 0.1);
        mutation = MutationFactory.getMutationOperator("BitFlipMutation", parameters);
        
        algorithm.addOperator("crossover",crossover);
        algorithm.addOperator("mutation",mutation);
        algorithm.addOperator("selection",selection);
        
        algorithm.setInputParameter("indicators", indicators);
        
        // Execute the Algorithm
        long initTime = System.currentTimeMillis();
        SolutionSet population = algorithm.execute();
        long estimatedTime = System.currentTimeMillis() - initTime;

        // Result messages 
        logger_.info("Total execution time: "+estimatedTime + "ms");
        logger_.info("Variables values have been writen to file VAR");
        population.printVariablesToFile("VAR");    
        logger_.info("Objectives values have been writen to file FUN");
        population.printObjectivesToFile("FUN");

        if (indicators != null) {
          logger_.info("Quality indicators") ;
          logger_.info("Hypervolume: " + indicators.getHypervolume(population)) ;
          logger_.info("GD         : " + indicators.getGD(population)) ;
          logger_.info("IGD        : " + indicators.getIGD(population)) ;
          logger_.info("Spread     : " + indicators.getSpread(population)) ;
          logger_.info("Epsilon    : " + indicators.getEpsilon(population)) ;  

          int evaluations = ((Integer)algorithm.getOutputParameter("evaluations")).intValue();
          logger_.info("Speed      : " + evaluations + " evaluations") ;      
        }
        
    }
}
