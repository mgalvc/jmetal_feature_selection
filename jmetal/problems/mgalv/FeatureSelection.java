package jmetal.problems.mgalv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.BinarySolutionType;
import jmetal.encodings.variable.Binary;
import jmetal.util.JMException;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.lazy.IBk;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

/**
 *
 * @author Matheus Galvão Correia
 */
public class FeatureSelection extends Problem {
    //formato do arquivo sempre .arff
    private File dataset;
    private int numberOfFeatures, classIndex;
    //private String[] classes;
    private DataSource source;
    
    public FeatureSelection(String path) {
        dataset = new File(path);
        numberOfFeatures = getNumberOfFeatures();
        numberOfVariables_ = 2;
        numberOfObjectives_ = 2;
        setClassIndex();
        
        solutionType_ = new BinarySolutionType(this);
        length_ = new int[numberOfVariables_];
        length_[0] = numberOfFeatures;
        
        classifier(null);
    }
    
    private int getNumberOfFeatures() {        
        try {
            DataSource source = new DataSource(dataset.getPath());
            Instances data = source.getDataSet();
            System.out.println(data.numAttributes() - 1);
            return data.numAttributes() - 1;
        } catch (Exception ex) {
            Logger.getLogger(FeatureSelection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return 0;
    }
    
    private void setClassIndex() {
        int n = 0;
        FileReader file;
        try {
            file = new FileReader(dataset);
            BufferedReader readFile = new BufferedReader(file);
            
            String line = readFile.readLine();
            String[] splittedLine = line.split(" ");
            
            while(!line.equals("@data")) {                
                if(splittedLine[0].toUpperCase().equals("@ATTRIBUTE")) {                    
                    if(splittedLine[1].equals("class")) {
                        classIndex = n;
                        break;
                    }
                    n++;
                }
                
                line = readFile.readLine();
                splittedLine = line.split(" ");
            }
            
        } catch (FileNotFoundException ex) {
            System.err.println("File not found");
        } catch (IOException ex) {
            Logger.getLogger(FeatureSelection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private double classifier(ArrayList<Integer> features) {
        double percentage = 0;
        
        try {
            DataSource source = new DataSource(dataset.getPath());
            Instances data = source.getDataSet();
            data.setClassIndex(classIndex);
            
            if (features != null)                
                for(int i = 0; i < features.size();) {
                    if(features.get(i) == 0) {
                        data.deleteAttributeAt(classIndex == 0 ? features.get(i) + 1 : features.get(i));
                        features.remove(i);                    
                    } else
                        i++;
                }
            
            Classifier cls = new IBk();
            cls.buildClassifier(data);
            
            Evaluation eval = new Evaluation(data);
            eval.crossValidateModel(cls, data, 10, new Random(1));
            
            percentage = eval.pctCorrect();        
        } catch (Exception ex) {
            Logger.getLogger(FeatureSelection.class.getName()).log(Level.SEVERE, null, ex);
        }
               
        System.out.println(percentage);
        return percentage;
    }

    @Override
    public void evaluate(Solution solution) throws JMException {
        Binary variable = (Binary) solution.getDecisionVariables()[0];
        int numberOfSelectedFeatures = 0;
        ArrayList<Integer> features = new ArrayList();
        
        for(int i = 0; i < variable.getNumberOfBits(); i++) {
            //pegar o número de características (minimizar)
            if(variable.bits_.get(i)) {
                numberOfSelectedFeatures++;
                features.add(1);
            }
            else
                features.add(0);
        }
        
        System.out.println(features.toString());

        //pegar o fitness do classifier (maximizar)
        double percentage = classifier(features);
        
        solution.setObjective(0, -1.0*percentage);
        solution.setObjective(1, numberOfSelectedFeatures);
    }       
}
