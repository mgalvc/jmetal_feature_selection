/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jmetal.problems.singleObjective;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.util.JMException;

/**
 *
 * @author matheus
 */
public class MyProblem extends Problem {
    
    public MyProblem() {
        numberOfConstraints_ = 0;
        numberOfObjectives_ = 1;
        numberOfVariables_ = 1;
        problemName_="MyProblem";
        
        solutionType_ = new RealSolutionType(this);
        
        upperLimit_ = new double[numberOfVariables_];
        lowerLimit_ = new double[numberOfVariables_];
        
        upperLimit_[0] = 2.0;
        lowerLimit_[0] = -1.0;
    }

    @Override
    public void evaluate(Solution solution) throws JMException {
        Variable variable = solution.getDecisionVariables() [0];
        double x = variable.getValue();
        
        double function = x*Math.sin(10*Math.PI*x) + 1;
        
        solution.setObjective(0, -function);
    }
    
    
}
